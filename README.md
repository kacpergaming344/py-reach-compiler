## **Py Reach Compiler**

## Images:

![UI!](https://i.imgur.com/1mdkp9x.png)
![UI2!](https://i.imgur.com/u80UyLr.png)
![LOGO!](https://i.imgur.com/lTggqSc.png)


## Description
@Py_Reach: Python to EXE Compiler

Py_Reach is a user-friendly Python script compilation tool designed to simplify the process of converting Python files (.py) into standalone executable files (.exe). The program offers several features to assist you in this task:

1. Select:

Function: This button enables you to choose a Python file from your local directory that you want to compile into an executable (.exe) file.
How to Use: Click "Select" to open a file dialog and select the Python script you wish to compile.

2. Execute:

Function: After selecting a Python script, click "Execute" to initiate the compilation process. Py_Reach will compile the chosen file into a single .exe file and store it in the 'py_output' folder, simplifying the distribution of your Python applications.
How to Use: First, click "Select" to choose a Python script, and then click "Execute" to compile it into an .exe and save it in the 'py_output' directory.

3. Console: ON/OFF:

Function: This button allows you to toggle the inclusion of a console window in the compiled .exe file. When set to "ON," the console is included, allowing you to view and interact with output messages. When set to "OFF," the console is excluded from the compiled file, making it suitable for GUI applications.
How to Use: Click "Console: ON/OFF" to enable or disable the console window as needed during the compilation process,
If you are doing console-based code then leave the CMD button on.

4. Recover:

Function: The "Recover" button provides a way to restore previously deleted or executed Python files in the .tmp format from the '.data' directory. Once recovered, the selected file will be copied to the 'recovered' folder with the .py extension.
How to Use: Click "Recover" to open a file dialog, locate and select the .tmp file from the '.data' directory, and recover it. The program will guide you through the recovery process.
With Py_Reach, you can easily compile your Python scripts into standalone .exe files, and its user-friendly interface simplifies the entire process. Whether you need console output or a clean GUI interface for your Python applications, Py_Reach offers you a simple and effective solution.

5. Manifest:

Function The "Manifest" button opens a window in which you can enter text and information about the code you are compiling ,
after compiling the code the manifest.txt file should appear next to it ,
To save the information in the manifest file press TAB

## **Download :**

**[int_Pyreach_r.zip](https://gitlab.com/kacpergaming344/py-reach-compiler/-/blob/main/int_Pyreach_r.zip?ref_type=heads) / Zip /**
**[files](https://gitlab.com/kacpergaming344/py-reach-compiler/-/tree/main/int_Pyreach_r?ref_type=heads) / Folder /**

**If Py Reach is not working try to run [requirements.bat](https://gitlab.com/kacpergaming344/py-reach-compiler/-/blob/main/requirements.bat?ref_type=heads)**

## Authors :
Me / ko2137

## License
I do not intend to publish the clean code of this program,
 in the near future.
